# Binar Car Rent - Chapter 7

Binar Car Rent is Challenge Chapter 7 given by Binar Academy.

To complete the requirements for making a challenge, you can see the link below :

- ReactJs `https://reactjs.org/docs/getting-started.html`
- Google Auth `https://console.cloud.google.com/`
- Redux `https://redux.js.org/`
- Backend `https://gitlab.com/sanlokaja/car-management-api.git`

## Account

- Email `akbar@gmail.com`
- Password `akbar123`
